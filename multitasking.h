#include "type.h"
#ifndef MULTITASKING
#define MULTITASKING

extern void wrapper32();

typedef struct stack {
    uint32 esp;
    uint32 edi, esi, ebp, esp2, ebx, edx, ecx, eax;
    uint32 eip, cs, eflags;
} Stack;

void create_task(uint32 fun);
int is_current_task_active();

typedef struct task_data {
    uint32 esp;
    uint8 is_active;
} Task;

uint32 switch_task(Stack stack);
void init_multitasking();
void exit();

#endif
