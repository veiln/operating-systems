%assign ICW1_MASTER 0b00010001
%assign ICW1_PORT_MASTER 0x20

%assign ICW2_MASTER 0b00100000
%assign ICW2_PORT_MASTER 0x21

%assign ICW3_MASTER 0b00000100
%assign ICW3_PORT_MASTER 0x21

%assign ICW4_MASTER 0b00000001
%assign ICW4_PORT_MASTER 0x21


%assign ICW1_SLAVE 0b00010001
%assign ICW1_PORT_SLAVE 0xa0

%assign ICW2_SLAVE 0b00101000
%assign ICW2_PORT_SLAVE 0xa1

%assign ICW3_SLAVE 0b00000010
%assign ICW3_PORT_SLAVE 0xa1

%assign ICW4_SLAVE 0b00000001
%assign ICW4_PORT_SLAVE 0xa1

%assign OCW1_PORT_MASTER 0x21
%assign OCW1_PORT_SLAVE 0xa1

%assign OCW2 0b00100000
%assign OCW2_PORT_MASTER 0x20
%assign OCW2_PORT_SLAVE 0xa0

%macro ICW 2
    mov al, ICW%1_%2
    mov dx, ICW%1_PORT_%2
    out dx, al
    call io_wait
%endmacro 

%macro PIC_CONFIGURE 1
    %assign i 1
    %rep 4
        ICW i, %1 
        %assign i i + 1
    %endrep
%endmacro

%macro IMR 2
    mov al, %2
    mov dx, OCW1_PORT_%1
    out dx, al
    call io_wait
%endmacro

;void pic_configure();
global pic_configure
pic_configure:
    PIC_CONFIGURE MASTER
    PIC_CONFIGURE SLAVE
    IMR SLAVE, 0b11111111
    IMR MASTER, 0b11111111
    ret

%macro OCW 3 ; OCW number, slave/master, ir number
    mov al, OCW%1
    or al, %3
    mov dx, OCW%1_PORT_%2
    out dx, al
    call io_wait
%endmacro


;void imr(uint8 mask);
;set master imr register value
global imr 
imr:
    mov al, [esp + 4]
    IMR MASTER, byte al
    ret

global slave_imr 
slave_imr:
    mov al, [esp + 4]
    IMR SLAVE, byte al
    ret

%macro EOI 1
    mov al, OCW2
    mov dx, OCW2_PORT_%1
    out dx, al
    call io_wait
%endmacro

;void eoi(uint8 ir);
global eoi
eoi:
    mov al, [esp + 4]
    cmp al, 0x27
    jg .slave
    jmp .master
    .slave:
        EOI SLAVE
    .master:
        EOI MASTER
    ret

;void set_interrupt_flag();
;enable unmasked interrupts calling
global set_interrupt_flag
set_interrupt_flag:
    sti
    ret

;void clean_interrupt_flag();
;disable unmasked interrupts calling
global clean_interrupt_flag
clean_interrupt_flag:
    cli
    ret

io_wait:
    mov dx, 0x80
    mov al, 0
    out dx, al
    ret
