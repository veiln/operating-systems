#ifndef HANDLER
#define HANDLER

#include "type.h"

struct handler_regs {
    uint32 ds;
    uint32 edi, esi, ebp, esp, ebx, edx, ecx, eax;
    uint32 int_num, err_code;
    uint32 eip, cs, eflags;
};

typedef struct handler_regs handler_args;

#endif
