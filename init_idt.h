#ifndef IDT
#define IDT

#include "type.h"
#pragma once

struct entry {
    uint16 lo;
    uint16 selector;
    uint8 zeroes;
    uint8 flags;
    uint16 hi;
} __attribute__((packed));

typedef struct entry Entry;

struct idtr {
    uint16 limit;
    uint32 base_addr;
} __attribute__((packed));

void init_idt();

extern void load(uint32 addr);

extern void wrapper0();
extern void wrapper1();
extern void wrapper2();
extern void wrapper3();
extern void wrapper4();
extern void wrapper5();
extern void wrapper6();
extern void wrapper7();
extern void wrapper8();
extern void wrapper9();
extern void wrapper10();
extern void wrapper11();
extern void wrapper12();
extern void wrapper13();
extern void wrapper14();
extern void wrapper15();
extern void wrapper16();
extern void wrapper17();
extern void wrapper18();
extern void wrapper19();
extern void wrapper32();
extern void wrapper33();

void fill_out(uint8 num, uint32 handler);

#endif
