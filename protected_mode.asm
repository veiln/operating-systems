[BITS 16]
extern main
;org 0x500

cli
lgdt [gdt_addr]
mov eax, cr0
or eax, 1
mov cr0, eax

jmp 0x10:init_segment_registers

[BITS 32]
init_segment_registers:
    mov ax, 0x8
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov ss, ax
    mov gs, ax

call main

dead_end:
jmp dead_end

gdt:
    dd 0 ; null
    dd 0

    dd 0xffff
    dd 0xcf9200 ;data
    
    dd 0xffff
    dd 0xcf9a00 ;code

gdt_addr:
    dw gdt_addr - gdt - 1 ;limit
    dd gdt ;base addr
