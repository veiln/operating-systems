#ifndef LIB
#define LIB

#define PANIC(s) panic(__LINE__, __FILE__, (s))

void clear(int memory);
int strlen(char * string);
void print(char * string);
void print_n(int n);

void scroll();
void scroll_line(int line);
void clear_line(int line);
void panic(int do_panic_line, char * file, char *s);
void itoa(int number, char * result);
void memcpy(char * first, int flen, char * second, int slen, char *res);
void append(char * data, int length, char * result);
#endif
