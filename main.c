#include "init_idt.h"
#include "pic.h"
#include "pit.h"
#include "lib.h"
#include "multitasking.h"

void task1() {
    int i = 0;
    for (i = 0; i < 1024; ++i) {
        print("task1 is here\n");
    }
}

void task2() {
    int i = 0;
    for(i = 0; i < 1024; ++i) {
        print("task2 is here\n");
    }
}

void init() {
    print("Init task is running\n");
    create_task((uint32)task1);
    create_task((uint32)task2);
    while (1) {
        continue;
    }
}

int main() {
    clear(0xb8000);

    print("Initialize interrupt descriptor table..\n");
    init_idt();

    print("Configure pic controller..\n");
    pic_configure();


    print("Initialize timer..\n");
    init_pit();

    imr(0b11111100);
    print("Initialize multitasking..\n");
    init_multitasking();

    create_task((uint32)init);

    set_interrupt_flag();
    return 0;
}
