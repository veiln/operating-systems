#include "lib.h"
#include "lock.h"
#include "type.h"

#define VIDEO_MEMORY 0xb8000
#define SCREEN_WIDTH 80
#define SCREEN_HEIGHT 25

#define COLOR 0x02 //green

#define BYTES_PER_SYMBOL 2
#define TAB_WIDTH 4

#define MAX_LINE 24

int LINE = 0; //current line we write a value to
int POS = 0;

void clear_line(int line) {
    int memory = VIDEO_MEMORY + line * BYTES_PER_SYMBOL * SCREEN_WIDTH;
    int i = 0;
    for (i = 0; i < SCREEN_WIDTH; ++i) {
        *(char *)memory = ' ';
        memory += BYTES_PER_SYMBOL;
    }
}

void clear(int memory) {
    int i = 0;
    for (i = 0; i < SCREEN_HEIGHT; ++i) {
        clear_line(i);
    }
}

void scroll_line(int line) {
    if (line == 0 || line >= SCREEN_HEIGHT) {
        PANIC("The line is not scrollable\n");
    }

    clear_line(line - 1);

    int memory = VIDEO_MEMORY + (line - 1) * BYTES_PER_SYMBOL * SCREEN_WIDTH;
    int i = 0;
    for (i = 0; i < SCREEN_WIDTH; ++i) {
        *(char *)memory = *(char *)(memory + SCREEN_WIDTH * BYTES_PER_SYMBOL);
        ++memory;

        *(char *)memory = COLOR;
        ++memory;
    }
}

void scroll() {
    int i = 0;
    for (int i = 1; i < SCREEN_HEIGHT; ++i) {
        scroll_line(i);
    }
    clear_line(MAX_LINE);
}

void print_symbol(char c) {
    int memory = VIDEO_MEMORY + 
                 BYTES_PER_SYMBOL * LINE * SCREEN_WIDTH + 
                 BYTES_PER_SYMBOL * POS;

    *(char *)memory = c;
    ++memory;

    *(char *)memory = COLOR;
    ++memory;
}

void new_line() {
    LINE += 1;
    POS = 0;
}

void update_offset(int value) {
    POS += value;
    if (POS >= SCREEN_WIDTH) {
        new_line();
    }
}

void print(char *string) {
    uint32 eflags = 0;
    eflags = lock(eflags);

    if (LINE > MAX_LINE) {
        scroll();
        LINE = MAX_LINE;
        POS = 0;
    }
    
    int length = strlen(string);

    if (length > SCREEN_WIDTH) {
        length = SCREEN_WIDTH;
    }

    int i = 0;
    for (i = 0; i < length; ++i) {
        switch (string[i]) {
            case '\n':
                new_line();
                break;
            case '\t':
                update_offset(TAB_WIDTH);
                break;
            default:
                print_symbol(string[i]);
                update_offset(1);
        }
    }

    unlock(eflags);
}

void print_n(int n) {
    char num[256];
    itoa(n, num);
    print(num);
}

int strlen(char * string) {
    int i = 0;
    while (*string++ != '\0') {
        ++i;
    }

    return i;
}

void itoa(int number, char * result) {
    if (number == 0) {
        result[0] = '0';
        result[1] = '\0';
        return;
    }

    int i = 0;
    while (number != 0) {
        *result++ = (number % 10) + '0';
        number /= 10;
        ++i;
    }

    result[i] = '\0';

    char * r_string = result - i;
    int j = 0;
    char tmp = '0';

    for (j = 0; j < i / 2; ++j) {
        tmp = *--result;
        *result = *r_string;
        *r_string++ = tmp;
    }
}


void memcpy(char * first, int flen, char * second, int slen, char *res)
{
    int i = 0;
    for (i = 0; i < flen; ++i) {
        *res++ = first[i];
    }
    
    for (i = 0; i < slen; ++i) {
        *res++ = second[i];
    }
    
}

void append(char * data, int length, char * result) {
    int i = 0;
    for (i = 0; i < length; ++i) {
        *result++ = data[i];
    }
}

void panic(int panic_line, char * file, char *s)
{
    int length = strlen(file);
    
    char line[8];
    itoa(panic_line, line);
    int line_length = strlen(line);

    int size = strlen(s);
    char output[length + line_length + 2 + size];

    char space[1];
    *space = ' ';
    

    memcpy(file, length, space, 1, output);
    append(line, line_length, output + length + 1);
    append(space, 1, output + length + line_length + 1);
    append(s, size, output + length + line_length + 2);
    
    print(output);
}

