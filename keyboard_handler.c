#include "lib.h"
#include "pic.h"
#include "type.h"
#include "keyboard.h"

uint8 keyboard_read() {
    uint8 code;
    asm volatile("in $0x60, %0": "=a"(code));
    return code;
}

uint8 get_keyboard_status() {
    uint8 status;

    asm volatile("in $0x64, %0": "=a"(status));
    return status;
}

uint8 is_buffer_empty(uint8 status) {
    return !(status & 0x1);
}

void keyboard_handler() {
    uint8 status = get_keyboard_status();
    if (!is_buffer_empty(status)) {
        uint8 scan_code = keyboard_read();
        match(scan_code);
    }
    eoi(0x21);
}
