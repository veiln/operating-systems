extern switcher

global switch_
switch_:
    pushad
    push esp
    call switcher
    pop esp
    popad
    iret

