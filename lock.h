#ifndef LOCK
#define LOCK

static inline __attribute__ ((always_inline)) unsigned int lock(unsigned int eflags) {
    asm volatile("pushf");
    asm volatile("pop %0":"=g"(eflags));
    asm volatile("cli");
    return eflags;
}
static inline __attribute__ ((always_inline)) unsigned int unlock(unsigned int eflags) {
    asm volatile("push %0"::"g"(eflags));
    asm volatile("popf");
    return eflags;
}

#endif
