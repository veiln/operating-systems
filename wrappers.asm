extern handler
extern pit_handler
extern keyboard_handler

%macro HANDLER 2
    global wrapper%1
    wrapper%1:
    pushad

    %if %2 > 0
        push byte 0
    %endif
    push byte %1

    call handler

    %if %2 > 0
        add esp, 4
    %endif
    add esp, 4
    popad
    iret
%endmacro

%macro PIT_HANDLER 0
    global wrapper32
    wrapper32:
        pushad
        push esp
        call pit_handler
        pop esp
        popad
        iret
%endmacro

%macro KEYBOARD_HANDLER 0
    global wrapper33
    wrapper33:
        pushad
        call keyboard_handler
        popad
        iret
%endmacro

%assign NO_ERRORCODE 1
%assign ERRORCODE 0

HANDLER 0, NO_ERRORCODE
HANDLER 1, NO_ERRORCODE
HANDLER 2, NO_ERRORCODE
HANDLER 3, NO_ERRORCODE
HANDLER 4, NO_ERRORCODE
HANDLER 5, NO_ERRORCODE
HANDLER 6, NO_ERRORCODE
HANDLER 7, NO_ERRORCODE
HANDLER 8, ERRORCODE
HANDLER 9, NO_ERRORCODE
HANDLER 10, ERRORCODE
HANDLER 11, ERRORCODE
HANDLER 12, ERRORCODE
HANDLER 13, ERRORCODE
HANDLER 14, ERRORCODE
HANDLER 15, NO_ERRORCODE
HANDLER 16, NO_ERRORCODE
HANDLER 17, ERRORCODE
HANDLER 18, NO_ERRORCODE
HANDLER 19, NO_ERRORCODE
PIT_HANDLER
KEYBOARD_HANDLER

global load
load:
    mov eax, [esp + 4]
    lidt [eax]
    ret
