#include "keyboard.h"
#include "lib.h"
char * scan_codes[] = {" <DUMMY KEY> ", " <ESC> ", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "-", "=", " <BACKSPACE> ", "\t", 
                       "q", "w", "e", "r", "t", "y", "u", "i", "o", "p", "[", "]", "\n", " <LEFT CTRL> ", "a", "s", 
                       "d", "f", "g", "h", "j", "k", "l", ";", "'", "`", " <LEFT SHIFT> ", "\\", "z", "x", "c", "v", "b", 
                       "n", "m", ",", ".", "/", " <RIGHT SHIFT> ", " <NEVER PRESSED> ", " <LEFT ALT> ", " "};
void match(uint8 code) {
    if (code <= 0x40) {
        print(scan_codes[code]);
        return;
    } else if (code < 0x80) {
        print("Unknown key value\n");
        return;
    } else if (code < 0x40 + 0x80) {
        return;
    }
     
    print("Unknown key value\n");
}

