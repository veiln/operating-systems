import os

def size():
    info = os.stat('./main')
    return (info.st_size // 512) + 1

if __name__ == "__main__":
    print size()
