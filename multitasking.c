#include "multitasking.h"
#include "lib.h"
#include "pic.h"
#include "lock.h"
#include "switch.h"
#include "type.h"

#define MAX_NUMBER_OF_TASKS 8
#define STACK_SIZE 4096

#define ACTIVE 1
#define INACTIVE 0

#define SWITCH_TASK() switch_()

Task task[MAX_NUMBER_OF_TASKS];
uint8 stacks[MAX_NUMBER_OF_TASKS][STACK_SIZE];
int current_task = 0;

void init_multitasking() {
    int i = 0;

    uint32 eflags = 0;
    eflags = lock(eflags);

    for(i = 1; i < MAX_NUMBER_OF_TASKS; ++i) {
        task[i].is_active = INACTIVE;
    }

    task[current_task].is_active = ACTIVE;

    unlock(eflags);
}

uint32 push(int task_num, uint32 fun) {
    const int CELL_SIZE = 4;
    const int REGS = 8;
    const int CS = 0x10;
    const int EFLAGS = 0x200;
    uint32 stack_ptr = (uint32)&(stacks[task_num + 1]);

    stack_ptr -= CELL_SIZE;
    *((uint32*)stack_ptr) = (uint32)exit;
    stack_ptr -= CELL_SIZE;
    *((uint32*)stack_ptr) = (uint32)EFLAGS;
    stack_ptr -= CELL_SIZE;
    *((uint32*)stack_ptr) = (uint32)CS;
    stack_ptr -= CELL_SIZE;
    *((uint32*)stack_ptr) = fun;
    stack_ptr -= CELL_SIZE * REGS;
    return stack_ptr;
}

void create_task(uint32 fun) {
    int i = 0;

    uint32 eflags = 0;
    eflags = lock(eflags);
    for (i = 0; i < MAX_NUMBER_OF_TASKS; ++i) {
        if (task[i].is_active == INACTIVE) {

            task[i].is_active = ACTIVE;
            task[i].esp = push(i, fun);

            unlock(eflags);
            return;
        }
    }

    unlock(eflags);
    PANIC("Could not create a task\n");
}

uint32 switch_task(Stack stack) {

    uint32 eflags = 0;
    eflags = lock(eflags);
    task[current_task].esp = stack.esp;
    int i = 0;
    for (i = current_task + 1; i < MAX_NUMBER_OF_TASKS; ++i) {
        if (task[i].is_active != INACTIVE && i != current_task) {
            current_task = i;

            unlock(eflags);
            return task[i].esp;
        }
    }

    for (i = 0; i <= current_task; ++i) {
        if (task[i].is_active != INACTIVE && i != current_task) {
            current_task = i;

            unlock(eflags);
            return task[i].esp;
        }
    }

    unlock(eflags);
    PANIC("Could not switch\n");
}

void exit() {
    print("Exiting...\n");

    clean_interrupt_flag();
    task[current_task].is_active = INACTIVE;
    SWITCH_TASK();
}

int is_current_task_active() {
    return task[current_task].is_active;
}
