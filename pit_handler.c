#include "lib.h"
#include "pic.h"
#include "multitasking.h"
#include "type.h"

int TIMER = 0;

void pit_handler(Stack stack) {
    if (TIMER % 100 == 0 || !is_current_task_active()) {
        stack.esp = switch_task(stack);
    }

    ++TIMER;
    eoi(0x20);
}
