[BITS 16]
org 0x7c00

%macro init_segment_register 1-*
  mov ax, 0x0
  %rep %0
    mov %1, ax
  %endrep
%endmacro

init_segment_register ss, ds, es, fs, gs
jmp 0x0:init

init:
  
%macro print 1 
  mov [es:di], word %1
  add di, 2
%endmacro

%macro print_string 1-*
  %rep %0
    print 0x0200 + %1
    %rotate 1
  %endrep
%endmacro

mov ax, 0xb000
mov es, ax

mov di, 0x8000

.print_spaces:
  cmp cx, 80 * 25 * 2 ; screen size 
  je .print_string
  print 0x0120 ; space
  add cx, 2
  jmp .print_spaces

.print_string:
  mov di, 0x8000
  add di, 80 * 13 * 2 + 40 ; screen offset
  
  print_string 'H', 'e', 'l', 'l', 'o', ' ', 'w', 'o', 'r', 'l', 'd' 

.end:
  times 510 - ($ - $$) db 0
  dw 0xaa55

