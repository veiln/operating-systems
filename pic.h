#ifndef PIC_H
#define PIC_H
#include "type.h"

extern void pic_configure();
extern void set_interrupt_flag();
extern void clean_interrupt_flag();
extern void imr(uint8 mask); 
extern void slave_imr(uint8 mask); 
extern void eoi(uint8 ir);

#endif
