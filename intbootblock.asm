[BITS 16]
org 0x7c00

%macro init_segment_register 1-*
  mov ax, 0x0
  %rep %0
    mov %1, ax
    %rotate -1
  %endrep

%endmacro

init_segment_register ss, ds, es, fs, gs
jmp 0x0:init

init:
  
%macro print 2   ; char, count
    mov cx, %2   ; count
    mov al, %1   ; character
    mov ah, 0x09
    mov bh, 0x0  ; page
    mov bl, 0x02 ; color - green
    int 0x10
%endmacro

%macro set_cursor_position 2 ; (row, column)
    mov dh, %1  ;row
    mov dl, %2  ; column
    mov ah, 0x2 
    mov bh, 0x0 ; page
    int 0x10
%endmacro

%macro print_string 1-* ; symbols to print
  mov di, 35
  %rep %0
    mov cx, di
    set_cursor_position 0xc, cl
    print %1, 0x1
    %rotate 1
    inc di
  %endrep
%endmacro

set_cursor_position 0x0, 0x0
print ' ', 80 * 25
print_string 'H', 'e', 'l', 'l', 'o', ' ', 'w', 'o', 'r', 'l', 'd'

end:
  times 510 - ($ - $$) db 0
  dw 0xaa55

