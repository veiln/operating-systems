[BITS 16]
org 0x7c00

%macro init_segment_register 1-*
  mov ax, 0x0
  %rep %0
    mov %1, ax
  %rotate -1
  %endrep
%endmacro

init_segment_register ss, ds, es, fs, gs
jmp 0x0:init

init:

mov ax, 0x0
mov es, ax
mov bx, 0x0500 

mov ah, 0x2 ; read disk sector
mov al, 0x0f ; number of sectors to read
mov cl, 0x2 ; sector number, high two bits of cylinder
mov ch, 0x0 ; low 8 bits of cylinder number 
mov dh, 0x0 ; head number
mov dl, 0x80; drive number (first hard disk)
int 0x13

jmp 0x0500

.end:
  times 510 - ($ - $$) db 0
  dw 0xaa55

