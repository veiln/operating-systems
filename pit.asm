;void init_pit();
global init_pit
init_pit:
    mov al, 0b00110100 ; rate generator
    mov dx, 0x43
    out dx, al

    mov al, 0xff
    mov dx, 0x40
    out dx, al
    
    mov al, 0xff 
    mov dx, 0x40
    out dx, al
    ret
