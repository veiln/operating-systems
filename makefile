C_SRC = lib.c handler.c init_idt.c main.c pit_handler.c multitasking.c switcher.c keyboard.c keyboard_handler.c
C_OBJ = lib.o handler.o init_idt.o main.o pit_handler.o multitasking.o switcher.o keyboard.o keyboard_handler.o
ASM_SRC = protected_mode.asm wrappers.asm pic.asm pit.asm switch.asm
ASM_OBJ = protected_mode.o wrappers.o pic.o pit.o switch.o

$(C_OBJ) : %.o : %.c
	gcc -m32 -fno-pie -nostdlib -nodefaultlibs -nostartfiles -fno-builtin -Wno-int-to-pointer-cast -march=i386 -c -o $@ $<

$(ASM_OBJ) : %.o : %.asm
	nasm -felf -o $@ $<

obj: $(C_OBJ) $(ASM_OBJ)
.PHONY: obj

main: 
	make obj
	ld -T script.ld -o main $(C_OBJ) $(ASM_OBJ)
.PHONY: main



SIZE = $(shell python ./get_size.py)
bochs: bootloader
	dd if=/dev/zero of=disk.img bs=1M count=1
	dd if=$< of=disk.img bs=1 count=512 conv=notrunc
	dd if=main of=disk.img bs=512 count=${SIZE} conv=notrunc seek=1
.PHONY: bochs

run:
	bochs -qf bochsrc
.PHONY: run

clean:
	rm -f $(C_OBJ) $(ASM_OBJ) main 
.PHONY: clean

