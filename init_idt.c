#include "init_idt.h"
#include "type.h"

Entry entries[256];
void init_idt() {
    struct idtr reg;
    reg.limit = sizeof(Entry) * 256 - 1;
    reg.base_addr = (uint32)&entries;
    
    load((uint32)&reg);
    
    fill_out(0, (uint32)wrapper0); 
    fill_out(1, (uint32)wrapper1); 
    fill_out(2, (uint32)wrapper2); 
    fill_out(3, (uint32)wrapper3); 
    fill_out(4, (uint32)wrapper4); 
    fill_out(5, (uint32)wrapper5); 
    fill_out(6, (uint32)wrapper6); 
    fill_out(7, (uint32)wrapper7); 
    fill_out(8, (uint32)wrapper8); 
    fill_out(9, (uint32)wrapper9); 
    fill_out(10, (uint32)wrapper10); 
    fill_out(11, (uint32)wrapper11); 
    fill_out(12, (uint32)wrapper12); 
    fill_out(13, (uint32)wrapper13); 
    fill_out(14, (uint32)wrapper14); 
    fill_out(15, (uint32)wrapper15); 
    fill_out(16, (uint32)wrapper16); 
    fill_out(17, (uint32)wrapper17); 
    fill_out(18, (uint32)wrapper18); 
    fill_out(19, (uint32)wrapper19);

    fill_out(32, (uint32)wrapper32);
    fill_out(33, (uint32)wrapper33);
}

void fill_out(uint8 num, uint32 handler) {
    entries[num].selector = 0x10;
    entries[num].zeroes = 0;
    entries[num].flags = 0x8e;
    entries[num].lo = handler & 0xffff;
    entries[num].hi = (handler & 0xffff0000) >> 16;
}
